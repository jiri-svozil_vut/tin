package sets;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class ReadFile {
    private String name_of_file;
    public ReadFile(String name_of_file){
        this.name_of_file = name_of_file;
    }
    public void read(){
        try {
            File myObj = new File(this.name_of_file);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
    public String getString(){
        try {
            String all = "";
            File myObj = new File(this.name_of_file);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                all = all + data;
            }
            myReader.close();
            return all;
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            return "";
        }

    }
    public void getList(){
        String all = getString();


    }
}