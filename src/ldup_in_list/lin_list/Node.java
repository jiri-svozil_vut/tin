package ldup_in_list.lin_list;

public class Node {
    private String data;
    private Node next_up;
    private Node next_down;



    public Node() {
    }

    public Node(String data) {
        this.data = data;
    }

    public Node(String data, Node next_up) {
        this.data = data;
        this.next_up = next_up;
    }

    public Node(String data, Node next_up, Node next_down) {
        this.data = data;
        this.next_up = next_up;
        this.next_down = next_down;
    }


    public Node getNext_up() {
        return next_up;
    }

    public void setNext_up(Node next_up) {
        this.next_up = next_up;
    }

    public Node getNext_down() {
        return next_down;
    }

    public void setNext_down(Node next_down) {
        this.next_down = next_down;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return data + " " + next_up;
    }
}
