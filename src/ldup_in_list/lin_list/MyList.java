package ldup_in_list.lin_list;

public class MyList {
    private Node first;
    private Node last;

    public void add_node(Node node){
        if (this.first == null){
            Node node_n = new Node();
            node_n.setData(node.getData());
            first = node_n;
            last = node_n;

        }
        else {
            Node node_n = new Node();
            node_n.setData(node.getData());
            last.setNext_up(node_n);
            node_n.setNext_down(last);
            last = node_n;
        }
    }
    public void delete_node_last(){
        last = last.getNext_down();
        last.setNext_down(null);
    }
    public void delete_node_first(){
        first = first.getNext_up();
        first.setNext_up(null);
    }
    public boolean is_empty(){
        if(this.first == null){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean is_in(String data){
        Node node = first;
        while (node != null){
            if (node.getData() == data){
                return true;
            }
            else{
                node = node.getNext_up();
            }
        }
        return false;
    }

    public void reverse(){
        Node node_fisrt = first;
        Node node_last = last;
        while (node_fisrt != null){
            String n = node_fisrt.getData();
            node_fisrt.setData(node_last.getData());
            node_last.setData(n);
            node_fisrt = node_fisrt.getNext_up();
            node_last = node_last.getNext_down();
        }
    }

    public String info(){
        return "Last: " + last.getData() + " | First: " + first.getData();
    }

    @Override
    public String toString() {
        return "" + this.first;
    }
}
