package ldup_in_list.lin_list;

public class Main {
    public static void main(String[] args) {
        MyList list = new MyList();
        list.is_empty();
        Node node1 = new Node("Node1");
        Node node2 = new Node("Node2", node1);
        Node node3 = new Node("Node3", node2);
        Node node4 = new Node("Node4", node3);
        Node node5 = new Node("Node5", node4);


        list.add_node(node1);
        list.add_node(node2);
        list.add_node(node3);
        list.add_node(node4);
        list.add_node(node5);

        System.out.println(list);
        System.out.println(list.is_in("Node3"));
        list.reverse();
        System.out.println(list.info());

        list.delete_node_last();
        System.out.println(list);
        System.out.println(list.info());

        list.delete_node_first();
        System.out.println(list);
        System.out.println(list.info());

    }
}
