package cz.vut.tin.cv6.graphs;

public class Edge {
    private Node node1, node2;

    public Edge(Node node1, Node node2, int value) {
        this.node1 = node1;
        this.node2 = node2;
        this.value = value;
    }

    private int value;

    public Node getNode1() {
        return node1;
    }


    public Node getNode2() {
        return node2;
    }

    public int getValue() {
        return value;
    }


    @Override
    public String toString() {
        return  node1 + " - " + node2 + " (" + value + ")" ;
    }


    public int compareTo(Edge compareEdge){
        return this.value - compareEdge.value;
    }
}
