package cz.vut.tin.cv6.graphs;

public class Node {

    private int id;


    public Node(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }



    @Override
    public String toString() {
        return ""+id;
    }
}
