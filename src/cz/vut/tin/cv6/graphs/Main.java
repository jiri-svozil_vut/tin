package cz.vut.tin.cv6.graphs;

public class Main {

    public static void main(String[] args) {
        Graph graph = new Graph();


        graph.addEdge(1, 2,4);
        graph.addEdge(2, 4,2);
        graph.addEdge(4, 3,4);
        graph.addEdge(1, 5,4);

        System.out.println(graph);

        Route route = new Route(graph, 1);
        route.addNode(2);
        route.addNode(4);
        route.addNode(3);



        System.out.println(route.getPrice());
    }
}
