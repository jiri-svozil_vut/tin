package cz.vut.tin.cv6.graphs;

import java.util.*;

public class Graph {
    private HashMap<Integer, Node> nodes = new HashMap<>();

    private HashSet<Edge> edges = new HashSet<>();


    public void addEdge(int idNode1, int idNode2, int value){
        Node node1 = ctrate_or_return_Node(idNode1);
        Node node2 = ctrate_or_return_Node(idNode2);

        Edge edge = new Edge(node1, node2, value);
        edges.add(edge);
    }

    private Node ctrate_or_return_Node(int idNode){
        Node newNode = nodes.get(idNode);
        if (newNode == null) {
            newNode = new Node(idNode);
            nodes.put(idNode, newNode);
        }
        return newNode;

    }



    public HashSet<Edge> getEdges() {
        return edges;
    }

    public HashMap<Integer, Node> getNodes() {
        return nodes;
    }



    public LinkedList<Edge> getSortEdges(){

        HashSet<Edge> mset =  new HashSet<>();
        mset.addAll(getEdges());
        int n = mset.size();
        LinkedList<Edge> ed = new LinkedList<>();
        for (Edge x : mset)
            ed.add(x);

        Collections.sort(ed, new Comparator<Edge>() {
            @Override
            public int compare(Edge o1, Edge o2) {
                return o1.getValue() - o2.getValue();
            }
        });

        return ed;

    }

    @Override
    public String toString() {
        return "Graph{" +
                "nodes=" + nodes +
                ", edges=" + edges +
                '}';
    }
}
