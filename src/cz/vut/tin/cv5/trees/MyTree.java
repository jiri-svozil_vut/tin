package cz.vut.tin.cv5.trees;

public class MyTree {
    private Node root;

    public void addNode(int data){
        this.root = addRecursive(root, data);
    }

    private Node addRecursive(Node current, int data){
        if (current == null) {
            return new Node(data);
        }
        if (data < current.getData()) {
            current.setLeft(addRecursive(current.getLeft(), data));
        } else if (data > current.getData()) {
            current.setRight(addRecursive(current.getRight(), data));
        } else {
            // value already exists
            return current;
        }

        return current;

    }

    public boolean searchData(int data){
        return serachRecursive(root, data);
    }

    private boolean serachRecursive(Node current, int data){
        if (current == null) {
            return false;
        }
        if (data == current.getData()) {
            return true;
        }
        return data < current.getData()
                ? serachRecursive(current.getLeft(), data)
                : serachRecursive(current.getRight(), data);
    }


    public void travelInOrder(){
        inOrder(root);
    }

    public void travelPreOrder(){
        preOrder(root);
    }

    public void travelPostOrder(){
        postOrder(root);
    }

    public void trevelLeafs(){
        leaf(root);
    }

    public void travelRevInOrder(){
        revInOrder(root);
    }

    public void travelRevPreOrder(){
        revPreOrder(root);
    }
    public void travelRevPostOrder(){
        revPostOrder(root);
    }

    public void printMaxDepth(){
        System.out.println(maxDepth(root));
    }


    private void inOrder(Node node){
        if (node != null) {
            inOrder(node.getLeft());
            System.out.print(" " + node.getData());
            inOrder(node.getRight());
        }
    }

    private void preOrder(Node node){
        if (node != null) {
            System.out.print(" " + node.getData());
            preOrder(node.getLeft());
            preOrder(node.getRight());
        }
    }

    private void postOrder(Node node){
        if (node != null) {
            postOrder(node.getLeft());
            postOrder(node.getRight());
            System.out.print(" " + node.getData());
        }
    }

    private void leaf(Node node){
        if (node == null){
            return;
        }
        if (node.getLeft() == null && node.getRight() == null){
            System.out.print(" " +  node.getData());
        }
        if (node.getLeft() != null)
            leaf(node.getLeft());

        if (node.getRight() != null)
            leaf(node.getRight());
    }


    private void revInOrder(Node node){
        if (node != null) {
            revInOrder(node.getRight());
            System.out.print(" " + node.getData());
            revInOrder(node.getLeft());
        }
    }

    private void revPreOrder(Node node){
        if (node != null) {
            System.out.print(" " + node.getData());
            revPreOrder(node.getRight());
            revPreOrder(node.getLeft());
        }
    }

    private void revPostOrder(Node node){
        if (node != null) {
            revPostOrder(node.getRight());
            revPostOrder(node.getLeft());
            System.out.print(" " + node.getData());
        }
    }

    public int maxDepth(Node node){
        if (node == null)
            return -1;
        else
        {
            int lDepth = maxDepth(node.getLeft());
            int rDepth = maxDepth(node.getRight());

            /* use the larger one */
            if (lDepth > rDepth)
                return (lDepth + 1);
            else
                return (rDepth + 1);
        }
    }

    @Override
    public String toString() {
        return "" + root;
    }
}
