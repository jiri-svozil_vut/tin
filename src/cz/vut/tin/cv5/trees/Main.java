package cz.vut.tin.cv5.trees;

public class Main {
    public static void main(String[] args) {
        MyTree bt = new MyTree();

        bt.addNode(7);
        bt.addNode(5);
        bt.addNode(1);
        bt.addNode(6);
        bt.addNode(14);
        bt.addNode(10);
        bt.addNode(16);
        bt.addNode(20);
        System.out.println(bt);

        System.out.print("Is 6 in: ");
        System.out.println(bt.searchData(6));
        System.out.print("Is 8 in: ");
        System.out.println(bt.searchData(8));

        System.out.println("In-Order:");
        bt.travelInOrder();

        System.out.println("\nPre-Order:");
        bt.travelPreOrder();

        System.out.println("\nPost-Order:");
        bt.travelPostOrder();


        System.out.println("\nLeafs:");
        bt.trevelLeafs();

        System.out.println("\nReverse In-Order:");
        bt.travelRevInOrder();

        System.out.println("\nReverse Pre-Order:");
        bt.travelRevPreOrder();

        System.out.println("\nReverse Post-Order:");
        bt.travelRevPostOrder();

        System.out.println("\nMax Depth:");
        bt.printMaxDepth();
    }


}
