package lin_list;

public class Main {
    public static void main(String[] args) {
        MyList list = new MyList();
        list.is_empty();
        Node node1 = new Node("Node1");
        Node node2 = new Node("Node2", node1);
        Node node3 = new Node("Node3", node2);


        list.add_node(node1);
        list.add_node(node2);
        list.add_node(node3);

        System.out.println(list);
        System.out.println(list.is_in("null"));
        list.delete_node();
        System.out.println(list);
    }
}
