package lin_list;

import java.util.ArrayList;

public class MyList {
    private Node first;

    public void add_node(Node node){
        if (this.first == null){
            first = new Node();
            first.setData(node.getData());
            first.setNext(null);
        }
        else {

            first.setData(node.getData());
            first.setNext(node.getNext());
        }
    }
    public void delete_node(){
        first = first.getNext();
    }
    public boolean is_empty(){
        if(this.first == null){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean is_in(String data){
        Node node = first;
        while (node != null){
            if (node.getData() == data){
                return true;
            }
            else{
                node = node.getNext();
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "" + this.first;
    }
}
