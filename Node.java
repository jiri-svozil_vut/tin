public class Node {

    private int data;
    private Node left;
    private Node right;

    private int dep;

    public int getDep() {
        return dep;
    }

    public void setDep(int dep) {
        this.dep = dep;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Node(int data, int dep) {
        this.data = data;
        this.dep = dep;
    }

    @Override
    public String toString() {
        return "Node{" +
                "dep=" + dep +
                ", data=" + data +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}
