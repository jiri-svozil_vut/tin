public class Main {



    public static void main(String[] args) {
        MyTree tree = new MyTree();
        tree.add(10);
        tree.add(4);
        tree.add(1);
        tree.add(6);
        tree.add(5);
        tree.add(7);
        tree.add(15);
        tree.add(20);

        System.out.println(tree);

        System.out.println(tree.getAvg());
        System.out.println(tree.getAvg_in_leafs());

        System.out.println(tree.getAvg());
        System.out.println(tree.getAvg_in_leafs());

        System.out.println(tree.getMaxValue());

        tree.printLevel(2);
    }
}
