public class MyTree {
    private Node root;

    private int sum;
    private int num_node;
    private int sum_leaf;
    private int num_leaf;

    private int currentDep;
    public void add(int data){
        root = addRecursive(root, data);
    }

    private Node addRecursive(Node current, int data){
        if(current == null){
            return new Node(data, currentDep);
        }
        if(data < current.getData()){
            currentDep+=1;
            current.setLeft(addRecursive(current.getLeft(), data));
        }
        else if (data > current.getData()){
            currentDep+=1;
            current.setRight(addRecursive(current.getRight(), data));
        }
        else {
            return current;
        }
        return current;

    }



    public double getAvg(){
        sum = 0;
        num_node =0;
        sum(root);
        double average = (double) sum/num_node;
        return average;
    }


    private void sum(Node node){
        if (node != null){
            sum+=node.getData();
            sum(node.getLeft());
            sum(node.getRight());
            num_node+=1;
        }
    }

    public double getAvg_in_leafs(){
        sum_leaf = 0;
        num_leaf = 0;
        sum_in_leaf(root);
        double average = (double) sum_leaf/num_leaf;
        return average;
    }

    private void sum_in_leaf(Node node){
        if (node == null){
            return;
        }
        if (node.getLeft() == null && node.getRight() == null){
           sum_leaf+=node.getData();
           num_leaf+=1;
        }
        if (node.getLeft() != null)
            sum_in_leaf(node.getLeft());

        if (node.getRight() != null)
            sum_in_leaf(node.getRight());
    }





    public int getMaxValue(){
        return findMax(root);
    }


    private int findMax(Node node)
    {
        if (node == null)
            return Integer.MIN_VALUE;
        int res = node.getData();

        if(node.getLeft() != null && node.getRight() != null){
            int lres = findMax(node.getLeft());
            int rres = findMax(node.getRight());

            if (lres > res)
                res = lres;
            if (rres > res)
                res = rres;
        }

        return res;

    }


    void printLevel(int level)
    {
        printCurrentLevel(root, level);
    }

    void printCurrentLevel(Node node, int level)
    {
        if (node == null)
            return;
        if (level == 1)
            System.out.print(node.getData() + " ");
        else if (level > 1) {
            printCurrentLevel(node.getLeft(), level - 1);
            printCurrentLevel(node.getRight(), level - 1);
        }
    }





    @Override
    public String toString() {
        return "" + root;
    }
}
